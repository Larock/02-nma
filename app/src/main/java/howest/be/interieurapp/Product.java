package howest.be.interieurapp;

public class Product {


    private int id;
    private String name;
    private int price;
    private String category;
    private int stock;
    private String imageURL;
    private String brand;

    public int getId() {
        return id;
    }
    public String getName() {
        return name;
    }
    public int getPrice() {
        return price;
    }
    public String getCategory() {
        return category;
    }
    public int getStock() {
        return stock;
    }
    public String getImageURL() {
        return imageURL;
    }
    public String getBrand() {
        return brand;
    }
}
