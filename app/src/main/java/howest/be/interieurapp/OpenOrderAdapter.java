package howest.be.interieurapp;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

public class OpenOrderAdapter extends RecyclerView.Adapter <OpenOrderAdapter.MyViewHolder> {

    private List<String> list;
    private OnItemClickListener onItemClickListener;

    public OpenOrderAdapter(List<String> list, OnItemClickListener onItemClickListener){
        this.list = list;
        this.onItemClickListener = onItemClickListener;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int ViewType) {
        TextView textView = (TextView) LayoutInflater.from(parent.getContext()).inflate(R.layout.neworder_template, parent, false);
        MyViewHolder myViewHolder = new MyViewHolder(textView, onItemClickListener);

        return myViewHolder;
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        holder.orderInfo.setText(list.get(position));
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView orderInfo;
        OnItemClickListener onItemClickListener;

        public MyViewHolder(View itemview, OnItemClickListener onItemClickListener){
            super(itemview);
            orderInfo = itemview.findViewById(R.id.txtOrder);
            this.onItemClickListener = onItemClickListener;
            itemview.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            onItemClickListener.OnItemClick(getAdapterPosition());
        }
    }

    public interface OnItemClickListener{
        void OnItemClick(int position);
    }
}
