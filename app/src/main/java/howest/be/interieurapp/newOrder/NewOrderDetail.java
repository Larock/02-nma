package howest.be.interieurapp.newOrder;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import java.util.List;

import howest.be.interieurapp.LaravelInterieurApi;
import howest.be.interieurapp.Order;
import howest.be.interieurapp.Product;
import howest.be.interieurapp.ProductOrder;
import howest.be.interieurapp.R;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class NewOrderDetail extends AppCompatActivity {

    private TextView textView;
    private Button btnOpenForPicking;
    private String content;
    private String name;
    private String email;


    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.new_order_detail);

        textView = findViewById(R.id.txtNewOrderDetail);
        btnOpenForPicking = findViewById(R.id.btnOpenForPicking);
        String x = getIntent().getStringExtra("orderId");
        String orderId = x.substring(0, 2);

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("http://laravel.interieurapp.be/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        LaravelInterieurApi api = retrofit.create(LaravelInterieurApi.class);

        Call<Order> orderCall = api.getOrderById(orderId);
        Call<List<ProductOrder>> productOrderCall = api.getByOrderId(orderId);



        orderCall.enqueue(new Callback<Order>() {
            @Override
            public void onResponse(Call<Order> call, Response<Order> response) {
                Order selectedOrder = response.body();
                content = "Name: " + selectedOrder.getName() + "\n";
                content += "Email: " + selectedOrder.getEmail() + "\n";

                name = selectedOrder.getName();
                email = selectedOrder.getEmail();
            }

            @Override
            public void onFailure(Call<Order> call, Throwable t) {

            }
        });

        productOrderCall.enqueue(new Callback<List<ProductOrder>>() {
            @Override
            public void onResponse(Call<List<ProductOrder>> call, Response<List<ProductOrder>> response) {
                List<ProductOrder> productOrders = response.body();
                content += "Products: " + productOrders.size();
                textView.setText(content);
            }

            @Override
            public void onFailure(Call<List<ProductOrder>> call, Throwable t) {

            }
        });

        btnOpenForPicking.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int id = Integer.parseInt(orderId);
                Order order = new Order(id, name, email, "In progress");

                Call<Order> updateCall = api.updateStatus(orderId, order);

                updateCall.enqueue(new Callback<Order>() {
                    @Override
                    public void onResponse(Call<Order> call, Response<Order> response) {
                        finish();
                    }

                    @Override
                    public void onFailure(Call<Order> call, Throwable t) {

                    }
                });
            }
        });
    }
}
