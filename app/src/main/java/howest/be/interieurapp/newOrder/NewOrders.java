package howest.be.interieurapp.newOrder;

import android.content.Intent;
import android.os.Bundle;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.View;

import java.util.ArrayList;
import java.util.List;

import howest.be.interieurapp.LaravelInterieurApi;
import howest.be.interieurapp.Order;
import howest.be.interieurapp.R;
import howest.be.interieurapp.RecyclerAdapter;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class NewOrders extends AppCompatActivity implements RecyclerAdapter.OnItemClickListener {

    private RecyclerView recyclerView;
    private RecyclerView.LayoutManager layoutManager;
    private ArrayList<String> orders = new ArrayList<>();
    private RecyclerAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.new_orders);

        recyclerView = findViewById(R.id.recyclerviewNeworders);
        layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("http://laravel.interieurapp.be/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        LaravelInterieurApi api = retrofit.create(LaravelInterieurApi.class);

        Call<List<Order>> call = api.getNewOrders();

        call.enqueue(new Callback<List<Order>>() {
            @Override
            public void onResponse(Call<List<Order>> call, Response<List<Order>> response) {
                List<Order> newOrders = response.body();

                for(Order order : newOrders){
                    String neworder = "Order " + order.getId() + ": " + order.getName();
                    orders.add(neworder);
                }
                sendList();
            }


            @Override
            public void onFailure(Call<List<Order>> call, Throwable t) {

            }
        });



    }
    public void sendList(){
        adapter = new RecyclerAdapter(orders, this);
        recyclerView.setHasFixedSize(true);
        recyclerView.setAdapter(adapter);
    }

    @Override
    public void OnItemClick(int position) {
        String order = orders.get(position);
        String[] split = order.split("\\s+");

        Intent intent = new Intent(this, NewOrderDetail.class);
        intent.putExtra("orderId", split[1]);
        startActivity(intent);
    }
}
