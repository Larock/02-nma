package howest.be.interieurapp;

import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.PUT;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface LaravelInterieurApi {

    @GET("api/products/getAll")
    Call<List<Product>> getProducts();

    @GET("api/orders/getNew")
    Call<List<Order>> getNewOrders();

    @GET("api/orders/getOpen")
    Call<List<Order>> getOpenOrders();

    @GET("api/orders/getOrderById/{id}")
    Call<Order> getOrderById(@Path("id") String id);

    @GET("api/orders/getByOrderId/{id}")
    Call<List<ProductOrder>> getByOrderId(@Path("id") String id);

    @PUT("api/orders/updateStatus/{id}")
    Call<Order> updateStatus(@Path("id") String id, @Body Order order);
}
