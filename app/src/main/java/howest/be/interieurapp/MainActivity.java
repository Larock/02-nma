package howest.be.interieurapp;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.util.List;

import howest.be.interieurapp.newOrder.NewOrders;
import howest.be.interieurapp.openOrder.OpenOrders;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MainActivity extends AppCompatActivity {

    private Button btnNewOrders;
    private Button btnOpenOrders;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_content);

        btnNewOrders = findViewById(R.id.btnNewOrders);
        btnOpenOrders = findViewById(R.id.btnOpenOrders);

        btnNewOrders.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                openNewOrderActivity();
            }
        });

        btnOpenOrders.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openOpenOrderActivity();
            }
        });
    }

    public void openNewOrderActivity(){
        Intent intent = new Intent(this, NewOrders.class);
        startActivity(intent);
    }

    public void openOpenOrderActivity(){
        Intent intent = new Intent(this, OpenOrders.class);
        startActivity(intent);
    }
}
