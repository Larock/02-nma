package howest.be.interieurapp.openOrder;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import howest.be.interieurapp.LaravelInterieurApi;
import howest.be.interieurapp.OpenOrderAdapter;
import howest.be.interieurapp.Order;
import howest.be.interieurapp.R;
import howest.be.interieurapp.RecyclerAdapter;
import howest.be.interieurapp.newOrder.NewOrderDetail;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class OpenOrders extends AppCompatActivity implements OpenOrderAdapter.OnItemClickListener{

    private RecyclerView recyclerView;
    private RecyclerView.LayoutManager layoutManager;
    private ArrayList<String> orders = new ArrayList<>();
    private OpenOrderAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.open_orders);

        recyclerView = findViewById(R.id.recyclerviewOpenorders);
        layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("http://laravel.interieurapp.be/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        LaravelInterieurApi api = retrofit.create(LaravelInterieurApi.class);

        Call<List<Order>> call = api.getOpenOrders();

        call.enqueue(new Callback<List<Order>>() {
            @Override
            public void onResponse(Call<List<Order>> call, Response<List<Order>> response) {
                List<Order> openOrders = response.body();

                for(Order order : openOrders){
                    String openOrder = "Order " + order.getId() + ": " + order.getName();
                    orders.add(openOrder);
                }
                Log.d("TAG", "onResponse: " + orders);
                sendList();
            }

            @Override
            public void onFailure(Call<List<Order>> call, Throwable t) {

            }
        });
    }

    public void sendList(){
        adapter = new OpenOrderAdapter(orders, this);
        recyclerView.setHasFixedSize(true);
        recyclerView.setAdapter(adapter);
    }

    @Override
    public void OnItemClick(int position) {
        String order = orders.get(position);
        String[] split = order.split("\\s+");

        Intent intent = new Intent(this, OpenOrderPicking.class);
        intent.putExtra("orderId", split[1]);
        startActivity(intent);
    }
}
